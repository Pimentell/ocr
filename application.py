
# Modulos para aplicacion
from flask import Flask, jsonify, request, render_template, Response
from flask_restful import Resource, Api
from flask_cors import CORS
# Modulos de tratamiento de imagen
from pdf2image import convert_from_bytes
from utils.image_processing import image_to_byte_array
from werkzeug.utils import secure_filename
from PIL import Image
import os 
from io import BytesIO
#Modulos internos
# from env.env_var import env
from models.main_aws import extract


application = app = Flask(__name__, template_folder= "templates")
CORS(application)
api = Api(app)

class RFC(Resource):
    def get(self):
        html = render_template("main.html")
        return Response(html, mimetype= "text/html")

    def allowedFile(self, filename):
        ALLOWED_EXTENSIONS  = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif']) 
        extension = filename.rsplit(".",1)[1].lower()
        return "." in filename and extension in ALLOWED_EXTENSIONS

    def post(self): 
        try:
            # Valid API KEYS : 5gB8UHHVdmGWwz8Z6N0UfYx8pvag1OyB, FMKxioOSez49ygFZV9ppIKJpMkElsXbX, xuE5qaC6ifoV8crTVPszAPbSuKIlp04q, g1MAYjL3qWryaCpGghNLXq4sspJB48Qo
            if request.form['API_KEY'] == "g1MAYjL3qWryaCpGghNLXq4sspJB48Qo": 
                self.api_key = request.form['API_KEY']
            else: 
                resp = jsonify({"message": "The request has no permissions"})
                resp.status_code = 400
                return resp
        except: 
            resp = jsonify({"message": "The request has no permissions"})
            resp.status_code = 400
            return resp
        if "file" not in request.files: 
            resp = jsonify({'message' : 'Can not find a file to import'})
            resp.status_code = 400
            return resp
        file = request.files['file']
        if file.filename == '':
            resp = jsonify({'message' : 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and self.allowedFile(file.filename):
            imagen  = convert_from_bytes(file.read(), last_page= 1, fmt = "JPEG",)[0]
            byte = image_to_byte_array(imagen)
            json_salida = extract(byte).tables
            resp = jsonify({"Message": "{}".format(json_salida)})
            resp.status_code = 200
            return resp

api.add_resource(RFC, "/")

if __name__== "__main__": 
    # env()
    app.run(debug= True)
""" Documentacion disponible"""
import boto3
import os

class extract():
    """
    Class for extract data and put into response
    """
    def __init__(self, document):
        self.session = boto3.Session(
            aws_access_key_id=os.environ['ACCESS_KEY'],
            aws_secret_access_key=os.environ['SECRET_ACCESS_KEY']
        )
        # Deprecated: Se ha optado por hacer el request a traves de la cadena de bytes en 
        # en lugar de un documento en S3. 
        self.s3 = self.session.client("s3")
        self.textract = self.session.client("textract", 
                                            region_name='us-east-1'
        )
        self.document_name = document
        self.extract()

    def extract(self):
        """
        El archivo debe enviarse como una imagen para poder hacer
        el procesamiento de los datos.
        El endpoint de Textract solo permite hacer analisis
        sincronico en formatos de jgp y png
        """
        response = self.textract.analyze_document(
            Document={
                "Bytes": self.document_name
            }, FeatureTypes=['TABLES'])
        blocks = response['Blocks']
        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block["Id"]] = block
            if block['BlockType'] == "TABLE": 
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        tables = []
        for table in table_blocks: 
            tables.append(self.__get_rows(table, blocks_map))
        
        # Para documentos que son constancias de RFC
        def getData(json): 
            """ 
            La tabla de extraccion de interes  en el caso de los RFC's es la segunda,
            de ahi se tomaran los datos de nombre, RFC, Razon Social. 
            como valor de Return ofrece un diccionario donde las llaves son: 
            - rfc
            - Denominacion/Razon Social
            - Regimen Capital
            - Nombre Comercial
            - Fecha de Inicio de Operaciones
            - Estatus en el padron
            - Fecha de ultimo cambio de estado
            
            V1. 
            dic = {json[0][i][1]:json[0][i][2] for i in json[0].keys()}

            V2. El envio de bytes para su extraccion provoca que no se reconozca la primer tabla.
            dic = {json[0][i][1]:json[0][i][2] for i in json[0].keys()}

            v3 . El valor de salida sera el diccionario completo, compuesto por todos los valores
            de la primer hoja del formato de RFC.
            """
            dic = {}
            for table in json: 
                for key in table.keys(): 
                    dic[table[key][1]]= table[key][2]
            return dic

        self.tables = getData(tables)
            


    def __getText(self, results, blocks_map):
        text = ""
        if "Relationships" in results: 
            for relationship in results['Relationships']: 
                if relationship["Type"] == "CHILD":
                    for child_id in relationship['Ids']: 
                        word = blocks_map[child_id]
                        if word['BlockType'] == "WORD": 
                            text += word['Text'] + " "
                        #En caso de que las tablas tengan formatos de Seleccion
                        #if word['BlockType'] == "SELECTION_ELEMENT": 
                        #   if word['BlockType'] == "SELECTED": 
                        #       text += "X"
        return text
    def __get_rows(self, table_result, blocks_map): 
        rows = {}
        for relationship in table_result['Relationships']: 
            if relationship['Type'] == "CHILD":
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == "CELL": 
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows: 
                            rows[row_index] = {}
                        rows[row_index][col_index] = self.__getText(cell, blocks_map)
        return rows

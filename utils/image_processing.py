import PIL
import io

def image_to_byte_array(imagen): 
    imgByteArr = io.BytesIO()
    imagen.save(imgByteArr, format = imagen.format)
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr